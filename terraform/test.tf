provider "yandex" {
  token     = file("token.txt")
  cloud_id  = "enpet2495rlslo46rgg6"
  folder_id = "b1gcgnkt2nbogf8qtd4q"
  zone      = "ru-central1-a"
}

resource "yandex_compute_instance" "apache" {
  name = "test"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd87va5cc00gaq2f5qfb"
    }
  }

  network_interface {
    subnet_id = "e9b4l27848sv8fegthp9"
    nat       = true
  }

  metadata = {
    user-data = file("user.txt")
  }
}

output "external_ip_address_apache" {
  value = yandex_compute_instance.apache.network_interface.0.nat_ip_address
}

